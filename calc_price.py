#!/usr/bin/env python3

import sys

t1,t2,price = sys.argv[1:]

with open(f'out/{t1[2:]}.cta') as f1:
    t1 = f1.read().strip().lower()
with open(f'out/{t2[2:]}.cta') as f2:
    t2 = f2.read().strip().lower()

if t1 < t2:
    print(int((1*float(price))**0.5 * 2**96))
else:
    print(int((1/float(price))**0.5 * 2**96))
