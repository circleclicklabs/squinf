//SPDX-License-Identifier:MIT
pragma solidity >=0.7.6;
contract StrUtil {
    function str2int(string memory x) public pure returns(uint256) {
	bytes memory b = bytes(x);
	uint256 result;
	for(uint256 n=0;n<b.length;++n){
	    if      (b[n]=='0'){
	    }else if(b[n]=='1'){result += 1;
	    }else if(b[n]=='2'){result += 2;
	    }else if(b[n]=='3'){result += 3;
	    }else if(b[n]=='4'){result += 4;
	    }else if(b[n]=='5'){result += 5;
	    }else if(b[n]=='6'){result += 6;
	    }else if(b[n]=='7'){result += 7;
	    }else if(b[n]=='8'){result += 8;
	    }else if(b[n]=='9'){result += 9;
	    }else{continue;}
	    result *= 10;
	}
	return result / 10 * 1 ether;
    }
    function str2float(string memory x) public pure returns(uint256) {
	bytes memory b = bytes(x);
	uint256 result;
	uint256 n;
	for(;n<b.length;++n){
	    if      (b[n]=='0'){
	    }else if(b[n]=='1'){result += 1;
	    }else if(b[n]=='2'){result += 2;
	    }else if(b[n]=='3'){result += 3;
	    }else if(b[n]=='4'){result += 4;
	    }else if(b[n]=='5'){result += 5;
	    }else if(b[n]=='6'){result += 6;
	    }else if(b[n]=='7'){result += 7;
	    }else if(b[n]=='8'){result += 8;
	    }else if(b[n]=='9'){result += 9;
	    }else if(b[n]=='.'){break;
	    }else{continue;}
	    result *= 10;
	}
	uint256 q = 1 ether / 10;
	result *= q;
	for(;n<b.length;++n){
	    if      (b[n]=='0'){
	    }else if(b[n]=='1'){result += q*1;
	    }else if(b[n]=='2'){result += q*2;
	    }else if(b[n]=='3'){result += q*3;
	    }else if(b[n]=='4'){result += q*4;
	    }else if(b[n]=='5'){result += q*5;
	    }else if(b[n]=='6'){result += q*6;
	    }else if(b[n]=='7'){result += q*7;
	    }else if(b[n]=='8'){result += q*8;
	    }else if(b[n]=='9'){result += q*9;
	    }else{continue;}
	    q /= 10;
	}
	return result;
    }    
}
