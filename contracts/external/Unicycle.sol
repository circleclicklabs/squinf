// SPDX-License-Identifier: MIT
pragma solidity ^0.7.6;
pragma abicoder v2;
import "@uniswap/v3-core/contracts/libraries/TickMath.sol";
import "@uniswap/v3-core/contracts/libraries/Position.sol";
import "@uniswap/v3-core/contracts/interfaces/IUniswapV3Pool.sol";
import "@uniswap/v3-periphery/contracts/libraries/TransferHelper.sol";
import "@uniswap/v3-periphery/contracts/libraries/LiquidityAmounts.sol";
library XferHelper {
    using TransferHelper for address;
    function safeXfer(address self,
		      address src, address dst, uint256 amt) internal {
	if (src == address(this)) self.safeTransfer    (/**/ dst, amt);
	else                      self.safeTransferFrom(src, dst, amt);}}
contract Callbacker {
    int24 internal constant MIN_TICK = -887272;
    int24 internal constant MAX_TICK = -MIN_TICK;
    uint160 internal constant MIN_SQRT_RATIO = 4295128739;
    uint160 internal constant MAX_SQRT_RATIO =
	1461446703485210103287273052203988822378723970342;
    uint160 internal constant MIN_PRICE = MIN_SQRT_RATIO + 1;   
    uint160 internal constant MAX_PRICE = MAX_SQRT_RATIO - 1;
    IUniswapV3Pool __pool = IUniswapV3Pool(address(0));}
contract UCallbacker is Callbacker {
    using XferHelper for address;
    function uniswapV3MintCallback(uint256 _amt0, uint256 _amt1,
				   bytes calldata _data) external {
	require(msg.sender == address(__pool));
	(address user) = abi.decode(_data, (address));
        if(_amt0>0) __pool.token0().safeXfer(user, msg.sender, uint256(_amt0));
        if(_amt1>0) __pool.token1().safeXfer(user, msg.sender, uint256(_amt1));
        __pool = IUniswapV3Pool(address(0));}}
contract SCallbacker is Callbacker {
    using XferHelper for address;
    function uniswapV3SwapCallback( int256 _amt0,  int256 _amt1,
				   bytes calldata _data) external {
	require(msg.sender == address(__pool));
	(address user) = abi.decode(_data, (address));
        if(_amt0>0) __pool.token0().safeXfer(user, msg.sender, uint256(_amt0));
        if(_amt1>0) __pool.token1().safeXfer(user, msg.sender, uint256(_amt1));
        __pool = IUniswapV3Pool(address(0));}}
contract LLMinter is UCallbacker {
    function mint(IUniswapV3Pool _pool, address _user,
		  int24 _tickLower, int24 _tickUpper,
		  bool direction, uint128 _liquidity) public {
        (, int24 _tickCurrent, , , , , ) = _pool.slot0();
	require(direction ? _tickCurrent < _tickLower
		          : _tickCurrent > _tickUpper, "RE"); // Range Error
	(__pool=_pool).mint(_user, _tickLower, _tickUpper,
			    _liquidity, abi.encode(msg.sender));}}
contract LLSwapper is SCallbacker {
    function swap(IUniswapV3Pool _pool, address _user, bool _fwd,
		  int256 _amt, uint160 _limit) public {
	(__pool=_pool).swap(_user, _fwd, _amt, _limit, abi.encode(_user));}}
contract Minter is LLMinter {
    using TickMath for int24;
    using LiquidityAmounts for uint160;
    function mint(IUniswapV3Pool _pool, address _user,
		  int24 _tickLower, int24 _tickUpper,
		  uint160 _priceLower, uint160 _priceUpper,
		  uint256 _amt0, uint256 _amt1) public {
        require(_amt0 < uint256(-1) && _amt1 < uint256(-1), "E2");
        (uint160 _priceCurrent, , , , , , ) = _pool.slot0();
	uint128 _liquidity = _priceCurrent
	    .getLiquidityForAmounts(_priceLower, _priceUpper,
				    _amt0, _amt1);
	require(_liquidity > 0, "EL");
	(__pool=_pool).mint(_user, _tickLower, _tickUpper,
			    _liquidity, abi.encode(msg.sender));}
    function mint(IUniswapV3Pool _pool, address _user,
		  int24 _tickLower, int24 _tickUpper,
		  uint256 _amt0, uint256 _amt1) public {
	mint(_pool, _user, _tickLower, _tickUpper,
	     _tickLower.getSqrtRatioAtTick(), _tickUpper.getSqrtRatioAtTick(),
	     _amt0, _amt1);}
    function mint(IUniswapV3Pool _pool, address _user, int24 _tickTarget,
		  uint256 _amt0, uint256 _amt1) public {
	int24 _tickSpacing = _pool.tickSpacing();
        int24 _tickLower = (_tickTarget / _tickSpacing) * _tickSpacing;
	//require(_tickLower == _tickTarget, "not on tick");
	int24 _tickUpper = _tickLower + _tickSpacing;
	mint(_pool, _user, _tickLower, _tickUpper, _amt0, _amt1);}}
contract Swapper is LLSwapper {
    function swap(IUniswapV3Pool _pool, address _user, int256 _amt) external {
	if (_amt>0) swap(_pool, _user,  true,  _amt, MIN_PRICE);
	else        swap(_pool, _user, false, -_amt, MAX_PRICE);}}
contract Burner {
    function burn(IUniswapV3Pool _pool,
		   int24 _tickLower, int24 _tickUpper, uint128 _liquidity)
	external returns (uint256 _amt0, uint256 _amt1) {
	return _pool.burn(_tickLower, _tickUpper, _liquidity);}}
contract Collector {
    function collect(IUniswapV3Pool _pool, address _user,
		     int24 _tickLower, int24 _tickUpper)
	public returns(uint256 _amt0, uint256 _amt1) {
	return _pool.collect(_user, _tickLower, _tickUpper,
			     type(uint128).max, type(uint128).max);}}
contract X96Utils {
    function x96(int24 _tick) public pure returns(uint160) {
	return TickMath.getSqrtRatioAtTick(_tick);}
    function tick(uint160 _X96Price) public pure returns(int24) {
	return TickMath.getTickAtSqrtRatio(_X96Price);}}
contract PoolUtils {
    function pos(IUniswapV3Pool pool, address user,
		   int24 tickLower, int24 tickUpper
		   ) external view returns(uint128 liquidity,
					   uint256 feeGrowthInside0LastX128,
					   uint256 feeGrowthInside1LastX128,
					   uint128 tokensOwed0,
					   uint128 tokensOwed1){
        return pool.positions(keccak256(abi.encodePacked
					(user, tickLower, tickUpper)));}
    function computePosKey(address user, int24 tickLower, int24 tickUpper
			   ) external pure returns(bytes32) {
        return keccak256(abi.encodePacked
			 (user, tickLower, tickUpper));}
    function computePoolKey(address _token0, address _token1, uint24 _fee
                            ) public pure returns (bytes32) {
        require(_token0 < _token1, "U1");
        return keccak256(abi.encode(_token0, _token1, _fee));}
    bytes32 internal constant POOL_INIT_CODE_HASH =
        0xe34f199b19b2b4f47f68442619d555527d244f78a3297ea89325f843f87b8b54;
    function computeAddress(address _factory,
                            address _token0, address _token1, uint24 _fee
                            ) public pure returns (address pool) {
        pool = address(uint256(keccak256(abi.encodePacked
                                         (hex'ff',
                                          _factory,
                                          computePoolKey(_token0,_token1,_fee),
                                          POOL_INIT_CODE_HASH))));}}
contract Unicycle is Swapper, Minter {}
contract UnicycleMax is Unicycle, Burner, Collector, X96Utils, PoolUtils {}
