// SPDX-License-Identifier: BUSL-1.1

pragma solidity =0.7.6;

import "../interfaces/IIndexOracle.sol";
import {SafeMath} from "@openzeppelin/contracts/math/SafeMath.sol";

contract IndexOracle is IIndexOracle {
    using SafeMath for uint256;

    uint256 public constant INDEX_SCALE = 1e4;

    address public controller = msg.sender;

    uint256 public override getRawIndex;
    
    function getIndex() public view override returns(uint256) {
	return getRawIndex.mul(getRawIndex).div(1 ether);
    }

    modifier onlyController() {
        require(msg.sender == controller, "Not controller");
        _;
    }

    /**
     * @notice init wPowerPerp contract
     * @param _controller controller address
     */
    function init(address _controller) external /*initializer*/ {
        //require(msg.sender == deployer, "Invalid caller of init");
        //require(_controller != address(0), "Invalid controller address");
        controller = _controller;
    }
    // source id 40 40
    // target date for latest
    function _setRawIndex(uint256 _amount) external /*onlyController*/ {
	getRawIndex = _amount;
    }

}
