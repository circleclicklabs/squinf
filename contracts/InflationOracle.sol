//SPDX-License-Identifier:MIT
pragma solidity >=0.7.6;
import "./interfaces/IIndexOracle.sol";
import "./StrUtil.sol";
interface ITruflationTester {
    function yoyInflation() external pure returns(string calldata);
}
contract InflationOracle is IIndexOracle, StrUtil {

    ITruflationTester oracle;

    uint256 public _rawIndex = 0;
    
    constructor(address _oracle) {
	_setOracle(_oracle);
    }
    function _setOracle(address _oracle) public {
	oracle = ITruflationTester(_oracle);
    }
    function _setRawIndex(uint256 _newRawIndex) public {
	_rawIndex = _newRawIndex;
    }
    function yoyInflation() public view returns(uint256) {
	if(_rawIndex > 0)
	    return _rawIndex;
	return _yoyInflation(oracle);
    }
    function _yoyInflation(ITruflationTester _oracle
			   ) public pure returns(uint256) {
	return str2float(_oracle.yoyInflation());
    }
    function getRawIndex() public view override returns(uint256) {
	return yoyInflation();
    }
    function getIndex() public view override returns(uint256) {
	uint256 inf = getRawIndex();
	return inf * inf / 1 ether;
    }
}
