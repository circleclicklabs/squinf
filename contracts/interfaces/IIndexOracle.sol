//SPDX-License-Identifier:MIT
pragma solidity >=0.7.6;
interface IIndexOracle {
    function getIndex() external view returns(uint256);
    function getRawIndex() external view returns(uint256);
}
