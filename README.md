# Squinf

A new powerperp that tracks Inflation² (squared).

Get it? ***SQU**ared **INF**lation!*

**Squinf** (SQUared INFlation[^1]) is a new financial derivative invented by the Research Team at ccl.io (insert team members) Squinf is one of the first Power Perpetuals and gives traders perpetual exposure to Inflation².

----

**Squinf** (SQUared INFlation[^1]) is a brand-new perpetual options protocol that provides unlimited upside leverage with no liquidations on long positions. Squinf isn't just a tool for traders, it can be used as a hedge for anyone holding dollar denomiated assets, including liquidity providers on Uniswap, and it can also provide a positive yield in flat inflationary scenarios. DeFi protocols can integrate squinth for more versitile structured finance products.

No long liquidations! No settlement times! No expirations!

In fact, since we only get an updated index value once per day, it can be used as a predictive value due to the "wisdom of crowds"[^5] In actuality you're measuring what the market anticipates inflation^2 will be, so you could take the square root and use that as a proxy for real inflation. Now you've gone from one data point per day to a continuous model.

## How does it work?

Mechanism-wise, Squinf functions similar to a perpetual swap, tracking the index of Truflation² rather than Truflation. It provides global options-like exposure (pure convexity, pure gamma) without the need for either strikes or expiries, effectively consolidating much of the potential options market liquidity into a single ERC20 token.

In short, Squinf makes options perpetual and is a very effective hedge against inflation for Uniswap LPs, any instrument denominated in USD, and anything that has a curved payoff. higher level DeFi protocols and strategies 

Users can mint as much Squinf (ERC20) as they want, using a CDP (collateralized debt position) NFT (ERC721). This means they are both long and short at the same time. Sell the squinf on the open market and now you're holding a short position.

To go long, a user simply buys Squinf directly and takes the risk.

Shorting for long periods is like a covered call, because you'll be collecting the premium unless inflation rises faster than expected.

Other strategies are possible, like,

  - Buy Squinf and a USD stablecoin, and stake them on Uniswap

The normalization factor in this kind of power perpetuals works as the funding mechanism. Since it's value is a function of the divergence of the mark price (price of the token in the market) and the index (ideal YoY Inflation rate). So, if the price is higher than the index price, longs will pay shorts. The payment works as "in kind" payment, reducing the debt value of the collateralized position.  

Risk vs reward: short term vehicle, not recommended for long periods. For long term bets on inflation, a less volatile token would probably be recommended.

The goal is to make a new financial instrument that can be leveraged and used by other DeFi[^2] and HyFi[^3] contracts / models / lending providers to gain flexibility in markets.


## Some Examples (Use cases)

## Going long

A user decides that inflation will rise extremely quickly in the coming months, and wants to prepare. For example, current YoY inflation is 2%.

  1. He goes to Uniswap V3 and looks for (say) the SQUINF-DAI pool with the best price. (the price now should trade around 2%)

  2. He swaps DAI for SQUINF and calls it a day.

Afterwards, the YoY inflation now is 5%. This means that if he didn't hedge the inflation risk, his acquisitive power will have decreased. However, since the user bought this instrument, he will have had a PnL of 5.27%. 

## Going short
A user decides inflation isn't going to rise anymore, and wants to short inflation. Now inflation is 10%.

  1. She opens a position using either a UI or the Squinf controller directly

  2. DAI is transferred from her account to the Squinf controller for collateral

  3. In exchange, she receives an NFT (ERC-721) that represents their position

  4. She also receives the requested amount of Squinf

  5. Now she's long and short, so she sells the Squinf on the open market

  6. Now she's only short, and returns are paid in terms of INF^2 (INF = Truflation)

  7. She can adjust the debt at anytime by depositing or withdrawing collateral

So, if inflation is now at 3%, the user would have generated a profit of 12.32% + funding fees.

## Betting sideways

A user decides inflation is going to stay more or less the same for a while (sideways market).

  1. They look on Uniswap for the Squinf-DAI market with the most liquidity

  1. They use DAI to swap for some Squinf and use them to stake a position on Uniswap

  1. When the market goes up and down within their liquidity endpoints, they earn fees.

  1. If they want to get *really* exotic, they can use the Uniswap position as collateral for a new Squinf position

  1. Now they have a new short AND a new long position

  1. They take the SQUINTH they received, and they stake that too for even more exposure!

## Architecture

### Primary Compoments

Smart contracts:

 - **DAI** (not ours): The USD token that we chose
 - **Squinf**: ERC20 token that represents your risk in terms of INF^2
 - **ShortPowerPerp**: ERC721 NFT that represents your position (Squinf and collateral)
 - **InflationOracle**: Interfaces with Truflation to provider the current numbers
 - **Controller**: primary interface
 - **UniswapV3Factory**( also not us): the core of the Uniswap v3 protocol
   Advanced Users may use Uniswap positions as collateral.
 - **SQUINF-DAI Pool (UniswapV3Pool)**: unbiased open market that we can get the prices from.  
   We can also buy and sell Squinf directly by just swapping.

Other Stuff:

- **NF**: *Normalization factor*. Represents the divergence betweeen the index and the trading price, working as a proxy for short term expectations.
- **lastComputed**: timestamp. Used together with NF recalculation. We're emulating a continuous function by recalculating it every time anything changes.
- **Vaults**: every position is its own vault. Vaults are also NFTs (ERC71). They hold your balances of Squinf and collateral. Everyone can see them, but only the user can access them.

- **Update Daemon**: Updates the Oracle directly from Truflation data once per day.

## How do we measure Inflation?

We all implicity know when inflation is happening, but we need a consistent way to *measure* inflation over time. 

Traditionally CPI (Consumer Price Index) has been used at the golden model, but it has some problems that make it less than desirable.

#### 1. CPI numbers only come out monthly

One could publish CPI numbers to the blockchain when they come out, but is that helpful? That is too slow for our purposes. We need, at a minumum, a daily update.

#### 2. The Fed has shifted methodology over time.

It's generally accepted that they don't lie or make up the numbers outright, but over time, the methodologies and methods change, as well as the data classifications (M1, M2, etc, there are more all the time). They "calibrate" and make "adustments" over time.

## Squinf exhibits positive convexity

Every derivatives position have some risk parameters called the greeks. Delta ($\delta$) and gamma ($\gamma$) are greeks related to the price movement of the underlying. Basically, $\delta$ is interpreted as the total amount the value of the positions is expected to move based on a 1\$ change. $\gamma$ on the other hand is defined as the rate of change in an option's delta per point move in the underlying asset's price. Gamma is an important measure of the convexity of a derivative's value. Convexity is a desired property in positions because it implies that for each unit decrease in price, the resulting price fall in the position is less than if it were linear. On the other hand, for each unit increase in price, the resulting price increase in the security is more than if it were linear relationship.

![](convexity.png)

## Influences

  - Squinf is heavily influenced by Squeeth, the first powerperp introduced by Opyn.

## (Possible) Future Directions:

#### Add Governance Model

  - Add Treasury/Insurance account

  - Add governance token and a DAO layer

  - Staking model for GOV token

#### Ability to select different asset classes

  - A Factory class to specify backing for different assets?
 
  - Multiple Assets for Collateral?

#### Make Vaults a seperate contract

  - It would just be a lot easier to update the controller

#### Strategies

  - use squinth as a lego block to build more complex strategies

  - reduce fees over do-it-yourself due to sharing of gas costs

#### Different Formulas / Curves

  - There's nothing stopping us from plugging in different formulas to compute the index

  - You can adjust risk/returns and graph out what the curve looks like

  - CUINF (cubed inflation), FOINF (FOurthed INFlation), and FIINF (FIfthed INFlation) are possible!

## Definitions  
  - [^1]Truflation: "actual" inflation as opposed to "reported" inflation
  - Powerperp: a Power Perpetual. Sort of like a non-expiring options with a floating strike price.
  - Normalization Factor (NF): 
  - Collateralize Debt Position (CDP): a short position that is backed by actual 
  - Total Value Locked (TVL): How much collateral is "owned" by the contract on the user's behalf (plus any extra slush)
  - [^2]DeFi: Decentralized Finance 
  - CeFi / TradFi: Centralized Finance / Traditional Finance
  - [^3]HyFi: Hybrid Finance, the marriage of DeFi and TradFi
  - Squeeth: Squared ETH is a derivative invented by the Research Team at Opyn (Zubin Koticha, Andrew Leone, Alexis Gauba, Aparna Krishnan), Dave White, and Dan Robinson. Squeeth is the first Power Perpetual and gives traders perpetual exposure to ETH².

[^9]: Actually, we're going to use Truflation to measure inflation. It could be any index on the blockchain, but we have chosen the most accurate.
