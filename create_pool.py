#!/usr/bin/env python3

import os, sys

s1,s2,fee,price = sys.argv[1:]

with open(f'out/{s1[2:]}.cta') as f1:
    t1 = f1.read().strip().lower()
    pass

with open(f'out/{s2[2:]}.cta') as f2:
    t2 = f2.read().strip().lower()
    pass

cmd = f'UniswapV3Factory createPool {s1} {s2} {fee}'
print("C", cmd)
r = os.system(cmd)
print("R", r)

cmd = f'UniswapV3Factory    getPool {s1} {s2} {fee} | ./lk.py save UniswapV3Pool --as Pool-{s1[2:]}-{s2[2:]} -'
print("C", cmd)
r = os.system(cmd)
print("R", r)

cmd = f'UniswapV3Factory    getPool {s1} {s2} {fee} | ./lk.py save UniswapV3Pool --as Pool-{s2[2:]}-{s1[2:]} -'
print("C", cmd)
r = os.system(cmd)
print("R", r)

if t1 < t2:
    p = (int((1*float(price))**0.5 * 2**96))
else:
    p = (int((1/float(price))**0.5 * 2**96))

cmd = f'Pool-{s1[2:]}-{s2[2:]} initialize {p}'
print("C", cmd)
r = os.system(cmd)
print("R", r)
