
TRUFLATIONORACLE=0x17dED59fCd940F0a40462D52AAcD11493C6D8073
JOBID=e5b99e0a2f79402998187b11f37c56a6
ONEPERCENT=10000000000000000

QUICKSTART=0x5fc949612bCf622A63C4D66B1aA132728Cc0eb1C

ZERO=0x0000000000000000000000000000000000000000
ONE=1000000000000000000
E18=000000000000000000
THOUSAND=1000000000000000000000

LIBS=--libraries ABDKMath64x64:`ABDKMath64x64 -a`,TickMathExternal:`TickMathExternal -a`,SqrtPriceMathPartial:`SqrtPriceMathPartial -a`,NFTDescriptor:`NFTDescriptor -a`

call:: clean all

full:: 3rd compile predeploy postcompile deploy deploy-controller

all:: 3rd postcompile2 deploy-controller

rcall:: realclean all

clean:
	find . -name '*~' | xargs rm -fr

tt::
	./lk.py save TruflationTester --as TruflationOracle ${TRUFLATIONORACLE}
	./lk.py deploy TruflationTester @@TruflationOracle ${JOBID} ${ONEPERCENT}

compile::
	find contracts -name '*.sol' | xargs solc.sh
	solc.sh contracts/InflationOracle.sol
	solc.sh @uniswap/v3-periphery/contracts/libraries/NFTDescriptor.sol
	solc.sh @uniswap/v3-core/contracts/UniswapV3Factory.sol --optimize-runs=1

predeploy::
	./lk.py deploy ABDKMath64x64
	./lk.py deploy TickMathExternal
	./lk.py deploy SqrtPriceMathPartial
	./lk.py deploy NFTDescriptor

postcompile::
	solc.sh @uniswap/v3-periphery/contracts/Nonfungible*.sol contracts/core/Controller.sol ${LIBS}

postcompile2::
	solc.sh contracts/core/Controller.sol ${LIBS}

deploy-adapter::
	./lk.py deploy InflationOracle ${QUICKSTART}
	InflationOracle _setRawIndex 107000000000000000000

deploy-base::
	./lk.py deploy UnicycleMax --as UC
	./lk.py deploy UniswapV3Factory
	./lk.py deploy WETH9
	./lk.py deploy NonfungibleTokenPositionDescriptor --as NFTPD @@WETH9 0x40404000
	./lk.py deploy NonfungiblePositionManager --as NFPM @@UniswapV3Factory @@WETH9 @@NFTPD
	./lk.py deploy RealOracle
	./lk.py deploy MockOracle

deploy-tokens::
	./lk.py deploy MockErc20      --as DAI DAI DAI 18
	./lk.py deploy WPowerPerp     --as LPP LPP LPP

deploy-pools::
	./create_pool.py @@LPP @@DAI 500 11449.10

deploy:: deploy-base deploy-adapter deploy-tokens deploy-pools

deploy-controller::
	./lk.py deploy ShortPowerPerp --as SPP SPP SPP
	./lk.py deploy Controller @@RealOracle @@InflationOracle @@SPP @@LPP @@DAI 500 @@NFPM

mint2::
	DAI mint ${PUBLIC} 5000${E18}
	DAI approve @@Controller 5000${E18}
	Controller mintWPowerPerpAmount 0 1${E18} 5000${E18} 0

compile-all:: 3rd
	find . -name '*.sol' | grep -v test | grep -v v2-core | grep -v audit | xargs solc.sh

3rd:: @chainlink @openzeppelin/contracts @openzeppelin/contracts-upgradeable base64-sol \
	@uniswap/v3-core @uniswap/v3-periphery @uniswap/v2-core @uniswap/lib

@chainlink:
	git clone https://github.com/smartcontractkit/chainlink.git @chainlink

@openzeppelin/contracts:
	mkdir -p @openzeppelin
	git clone https://github.com/val314159/openzeppelin-contracts
	mv openzeppelin-contracts/contracts $@
	rm -fr openzeppelin-contracts

@openzeppelin/contracts-upgradeable:
	mkdir -p @openzeppelin
	git clone https://github.com/val314159/openzeppelin-contracts-upgradeable
	mv openzeppelin-contracts-upgradeable/contracts $@
	rm -fr openzeppelin-contracts-upgradeable

@uniswap/v3-core:
	mkdir -p @uniswap
	cd @uniswap ; ln -s ../uniswap-v3-core v3-core

@uniswap/v2-core:
	mkdir -p @uniswap
	git clone https://github.com/uniswap/v2-core $@

@uniswap/v3-periphery:
	mkdir -p @uniswap
	git clone https://github.com/uniswap/v3-periphery $@

base64-sol:
	git clone https://github.com/Brechtpd/base64 $@

@uniswap/lib:
	mkdir -p @uniswap
	git clone https://github.com/uniswap/solidity-lib $@

realclean: clean
	rm -fr @openzeppelin base64-sol out @uniswap
